package com.example.audiorecpause;

public interface RecordAudioComplement {

	/**
	 * Comienza la grabacion o reanuda la grabacion
	 */
	public void startRecord();
	
	/**
	 * Pausa la grabacion
	 */
	public void pauseRecord();
	/**
	 * Detiene la grabacion y retorna un boolean(true = creado , false = no creado)
	 */
	public boolean stopRecord();
	/**
	 * Le da un nombre de la carpeta donde se almacenara los audios.
	 * @param nameDirectory String 
	 */
	public void setNameDirectory(String nameDirectory);
	/**
	 * Le da la ruta donde se encontrara la capeta con audios.
	 * @param path String 
	 */
	public void setPath(String path);
	/**
	 * Le da el nombre del archivo de audio en formato WAV que sera
	 * almacenado en la carpeta de audios.
	 * @param fileOut String
	 */
	public void setNameOutFileAudio(String fileOut);
}
