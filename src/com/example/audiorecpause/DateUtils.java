package com.example.audiorecpause;

import java.util.Date;

public class DateUtils {

	/**
	 * Metodo return date ano/mes/dia/hora/min/seg
	 * @return
	 */
	public static String getLocalId(){

		Date date = new Date();

		String time = date.getYear()+"_"+date.getMonth()+"_"+date.getDay()+"_"+
		date.getHours()+"_"+date.getMinutes()+"_"+date.getSeconds();
		if(time != null && !time.equals("")){
			return time;
		}else{
			return "1";
		}
	}
}
