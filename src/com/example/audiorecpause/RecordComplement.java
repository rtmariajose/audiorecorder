package com.example.audiorecpause;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

public class RecordComplement implements RecordAudioComplement{

	private String PATH;
	private String NAMEDIRECTORY;
	private String AUDIO_RECORDER_TEMP_FILE;
	private String AUDIO_RECORDER_FINAL ;

	private static final int RECORDER_SAMPLERATE = 44100;
	private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO;
	private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

	private ArrayList<String> audiosString = null;
	private AudioRecord recorder = null;
	private Thread recordingThread = null;
	private FileOutputStream audioFileOut = null;
	private int bufferSize = 0;
	private boolean recording = false;

	/**
	 * 
	 */
	public RecordComplement(){
		audiosString = new ArrayList<String>();
		bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,RECORDER_CHANNELS,RECORDER_AUDIO_ENCODING);
	}

	@Override
	public void startRecord() {

		//		Toast.makeText(getApplicationContext(), "Grabandoo...", Toast.LENGTH_LONG).show();
		AUDIO_RECORDER_TEMP_FILE  = createFileTemporal();
		audiosString.add(AUDIO_RECORDER_TEMP_FILE);

		recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
				RECORDER_SAMPLERATE, RECORDER_CHANNELS,RECORDER_AUDIO_ENCODING, bufferSize);
		recorder.startRecording();
		recording = true;
		recordingThread = new Thread(new Runnable() {

			@Override
			public void run() {
				writeDataAudioFile();
			}
		},"AudioRecorder Thread");

		recordingThread.start();
	}

	/**
	 * Cuando presiona pause, se detiene la grabacion, dejando las 
	 * variables en stopy null del recorder y recorder
	 */
	@Override
	public void pauseRecord() {
		if(null != recorder){
			recording = false;
			recorder.stop();
			recorder.release();
			recorder = null;
			recordingThread = null;
		}

	}

	@Override
	public boolean stopRecord() {
		
		if(null != recorder){
			recording = false;
			recorder.stop();
			recorder.release();
			recorder = null;
			recordingThread = null;
		}

		//se unen todos los archivos que se encuentran en la lista 
		return mergeAudio();

	}


	@Override
	public void setNameDirectory(String nameDirectory) {
		this.NAMEDIRECTORY = nameDirectory;
	}

	@Override
	public void setPath(String path) {
		this.PATH = path;
	}

	@Override
	public void setNameOutFileAudio(String fileOut) {
		this.AUDIO_RECORDER_FINAL = fileOut;
	}

	/**
	 * Creacion de la carpeta con la ruta especificada con el path , el 
	 * nombre de la carpeta mas el nombre del audio sin la extension, porque
	 * la extension sera siempre *.wav.
	 * @return String 
	 */
	private String creationFileAudio(){

		File f = new File(PATH,NAMEDIRECTORY);

		if(!f.exists()){
			f.mkdirs();
		}
		return f.getAbsolutePath()+"/"+AUDIO_RECORDER_FINAL+".wav";

	}

	/**
	 * Creacion de archivos de audio temporales necesarios para el Record de
	 * audio que se esta realizando.
	 * @return String
	 */

	private String createFileTemporal(){

		File file = new File(PATH, NAMEDIRECTORY);
		if(!file.exists())
			file.mkdirs();
		String audioTempFile = DateUtils.getLocalId()+".raw";
		File tempFile = new File(PATH,audioTempFile);
		if(tempFile.exists())
			tempFile.delete();

		return (file.getAbsolutePath() + "/" + audioTempFile);
	}


	/**
	 * Escribe el archivo de audio  mientras se esta grabando
	 *  en el archivo temporal creado.
	 */
	private void writeDataAudioFile(){
		
		byte data[] = new byte[bufferSize];
		FileOutputStream os = null;
		try {
			os = new FileOutputStream(this.AUDIO_RECORDER_TEMP_FILE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int read = 0;

		if(null != os){
			while(recording){
				read = recorder.read(data, 0, bufferSize);

				if(AudioRecord.ERROR_INVALID_OPERATION != read){
					try {
						os.write(data);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	/**
	 * Se encarga  de escribir el encabezado del archivo de audio
	 * @param out
	 * @param totalAudioLen
	 * @param totalDataLen
	 * @param longSampleRate
	 * @param channels
	 * @param byteRate
	 * @throws IOException
	 */
	private void WriteWaveFileHeader(
			FileOutputStream out, long totalAudioLen,
			long totalDataLen, long longSampleRate, int channels,
			long byteRate) throws IOException {

		byte[] header = new byte[44];

		header[0] = 'R';  // RIFF/WAVE header
		header[1] = 'I';
		header[2] = 'F';
		header[3] = 'F';
		header[4] = (byte) (totalDataLen & 0xff);
		header[5] = (byte) ((totalDataLen >> 8) & 0xff);
		header[6] = (byte) ((totalDataLen >> 16) & 0xff);
		header[7] = (byte) ((totalDataLen >> 24) & 0xff);
		header[8] = 'W';
		header[9] = 'A';
		header[10] = 'V';
		header[11] = 'E';
		header[12] = 'f';  // 'fmt ' chunk
		header[13] = 'm';
		header[14] = 't';
		header[15] = ' ';
		header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
		header[17] = 0;
		header[18] = 0;
		header[19] = 0;
		header[20] = 1;  // format = 1
		header[21] = 0;
		header[22] = (byte) channels;
		header[23] = 0;
		header[24] = (byte) (longSampleRate & 0xff);
		header[25] = (byte) ((longSampleRate >> 8) & 0xff);
		header[26] = (byte) ((longSampleRate >> 16) & 0xff);
		header[27] = (byte) ((longSampleRate >> 24) & 0xff);
		header[28] = (byte) (byteRate & 0xff);
		header[29] = (byte) ((byteRate >> 8) & 0xff);
		header[30] = (byte) ((byteRate >> 16) & 0xff);
		header[31] = (byte) ((byteRate >> 24) & 0xff);
		header[32] = (byte) (2 * 16 / 8);  // block align
		header[33] = 0;
		header[34] = 16;  // bits per sample
		header[35] = 0;
		header[36] = 'd';
		header[37] = 'a';
		header[38] = 't';
		header[39] = 'a';
		header[40] = (byte) (totalAudioLen & 0xff);
		header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
		header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
		header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

		out.write(header, 0, 44);
	}

	/**
	 * 
	 * @param audio
	 */
	private void deleteFileAudio(String audio){

		File file = new File(audio);
		file.delete();
	}
	
	/**
	 * Une los archivos de audio
	 * @param inFilename
	 */
	private boolean mergeAudio(){

		long totalAudioLen = 0;
		long totalDataLen = 0;
		byte[] data = new byte[bufferSize];
		int channels = 2;
		long byteRate = 16 * RECORDER_SAMPLERATE * channels/8;
		long longSampleRate = RECORDER_SAMPLERATE;

		try {
			audioFileOut = new FileOutputStream(creationFileAudio());

			for (String fileAudio : this.audiosString) {
				FileInputStream in = new FileInputStream(fileAudio);
				totalAudioLen += in.getChannel().size();
				totalDataLen += totalAudioLen+36;
			}
			Log.i("Total tama�o de audio", ""+totalAudioLen);

			//una vez terminado se debe crear la cabecera
			WriteWaveFileHeader(audioFileOut, totalAudioLen, totalDataLen,
					longSampleRate, channels, byteRate);

			for (String fileAudio : this.audiosString) {
				Log.i("FOREACH- escritura de los archivos de audio en el archivo de salida", "OK");
				FileInputStream in = new FileInputStream(fileAudio);
				while(in.read(data) != -1){
					audioFileOut.write(data);
				}
				in.close();

				deleteFileAudio(fileAudio);
			}

			audioFileOut.close();
			Log.i("Creacion audio", "Se creo correctament el archivo de audio");
			this.audiosString.clear();//vacia la lista de audios
			return true;
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			//si halgo salio mal borra el archivo de la carpeta
			deleteFileAudio(creationFileAudio());
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			//si halgo salio mal borra el archivo de la carpeta
			deleteFileAudio(creationFileAudio());
			return false;
		}
	}


}
