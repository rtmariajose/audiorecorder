package com.example.audiorecpause;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

/**
 * 
 * @author MariaJose Rioseco
 *
 */
public class MainActivity extends Activity {

	RecordAudioComplement recAudioCom;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		recAudioCom = new RecordComplement();
		recAudioCom.setPath(Environment.getExternalStorageDirectory().getPath());
		recAudioCom.setNameDirectory("FilesAudio");
		recAudioCom.setNameOutFileAudio("audiosalidafinal");
	}


	public void recaudio(View v){

		recAudioCom.startRecord();
		Toast.makeText(getApplicationContext(), "Grabando....", Toast.LENGTH_LONG).show();
	}

	/**
	 * 
	 * @param v
	 */
	public void pauseaudio(View v){
		recAudioCom.pauseRecord();
		Toast.makeText(getApplicationContext(), "Pausado", Toast.LENGTH_LONG).show();
	}

	/**
	 * 
	 * @param v
	 */
	public void stopaudio(View v){
		boolean resultadoRecAudio = false;
		resultadoRecAudio = recAudioCom.stopRecord();
		Toast.makeText(getApplicationContext(), "Detenido", Toast.LENGTH_LONG).show();
		
		if(resultadoRecAudio)
			Toast.makeText(getApplicationContext(), "Se creao correctamente el archivo de audio", Toast.LENGTH_LONG).show();
		else
			Toast.makeText(getApplicationContext(), "No se creo el archivo de audio", Toast.LENGTH_LONG).show();

	}





}
